#ifndef LOGGER_H
#define LOGGER_H

#include <charconv.h>
#include <e32cons.h>
#include <f32file.h>

class Logger : public CBase
{
public:
	Logger::~Logger()
	{
		log_file.Flush();
		log_file.Close();
		session.Close();
	}
	
	static Logger* NewL(const TDesC& file_name)
	{
		_LIT(KEmpty, "");
		Logger* logger = NewLC(file_name, false, KEmpty);
		CleanupStack::Pop(logger);
		return logger;
	}

	static Logger* NewL(const TDesC& file_name, const TDesC& console_name)
	{
		Logger* logger = NewLC(file_name, true, console_name);
		CleanupStack::Pop(logger);
		return logger;
	}

	static Logger* NewLC(const TDesC& file_name, TBool create_console, const TDesC& console_name)
	{
		Logger* logger = new(ELeave) Logger();
		CleanupStack::PushL(logger);
		logger->ConstructL(file_name, create_console, console_name);
		return logger;
	}

	void log(TRefByValue<const TDesC> format, ...)
	{
		VA_LIST args;
		VA_START(args, format);

		// Convert to ASCII
		TBuf8<256> ascii;
		ascii.Copy(format);

		// Format the string and write it to the file
		TBuf8<256> buffer;
		buffer.FormatList(ascii, args);
		log_file.Write(buffer);

		// Also print the log message to the console, if possible.
		if (console)
		{
			TBuf16<256> wbuffer;
			wbuffer.Copy(buffer);
			console->Printf(wbuffer);
		}

		VA_END(args);
	}

	// Clears the current console line and replaces it.
	void console_line(TRefByValue<const TDesC> format, ...)
	{
		VA_LIST args;
		VA_START(args, format);

		// Convert to ASCII
		TBuf8<256> ascii;
		ascii.Copy(format);

		// Format the string
		TBuf8<256> buffer;
		buffer.FormatList(ascii, args);

		// Clear the current console line
		TPoint cursor_pos = console->CursorPos();
		console->SetCursorPosAbs(TPoint(0, cursor_pos.iY));
		console->ClearToEndOfLine();

		// Write it to the console
		TBuf16<256> wbuffer;
		wbuffer.Copy(buffer);
		console->Printf(wbuffer);
		
		VA_END(args);
	}

	// Prints to the console without formatting
	void console_raw(TRefByValue<const TDesC> text)
	{
		// Convert to ASCII
		TBuf8<256> ascii;
		ascii.Copy(text);

		TBuf16<256> wbuffer;
		wbuffer.Copy(ascii);
		console->Printf(wbuffer);
	}

	TKeyCode get_char()
	{
		if (console)
		{
			return console->Getch();
		}
		else
		{
			return EKeyNull;
		}
	}

private:
	Logger::Logger() : console(NULL)
	{
	}

	void ConstructL(const TDesC& file_name, TBool create_console, const TDesC& console_name)
	{
		// Create the console if requsted
		if (create_console)
		{
			console = Console::NewL(console_name, TSize(KConsFullScreen, KConsFullScreen));
		}

		// Open a session to the file server
		TInt result = session.Connect();

		if (result != KErrNone)
		{
			if (console)
			{
				_LIT(KConnectionFailure, "Failed to connect to file server: %d\n");
				console->Printf(KConnectionFailure, result);
				console->Getch();
			}

			User::Leave(result);
		}
		
		// Truncate/create the file for writing to.
		result = log_file.Replace(session, file_name, EFileShareExclusive | EFileWrite);

		if (result != KErrNone)
		{
			if (console)
			{
				_LIT(KLogOpenFailure, "Failed to open log file: %d\n");
				console->Printf(KLogOpenFailure, result);
				console->Getch();
			}
			
			User::Leave(result);
		}
	}
	
	CConsoleBase* console;
	RFs session;
	RFile log_file;
};

#endif