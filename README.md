# Rusty Starship
Rusty Starship is a collection of tools for the N-Gage gaming console.  
While unsupported, the tools should theoretically also work on other Symbian OS 6 devices.

The name is derived from N-Gage's development codename - Starship.

## Building
Symbian 1.2 SDK is required for building the tools.  
Run `bldmake bldfiles` and `ABLD.BAT build armi urel` in the directory of a given tool to build it.  
The resulting executable should be located in *\<SDK path>*/Epoc32/Release/armi/urel.

## Symbian dumper
Symbian dumper (SDumper for short) is a tool, which dumps the BOOT, ROM and ROOT sections of the device memory. It also logs a lot of other information about the device.  
About 20MBs of free space is required on the MMC.